<?php

/**
 * @file
 * Admin functions for webClinic Pro security module.
 *
 * @company webClinic Pro
 */

/**
 * Implements hook_admin_settings().
 */
function webclinicpro_admin_settings() {
  $form = array();

  if (!isset($_REQUEST['form_token'])) {
    if (variable_get('webclinicpro_status', 0) == 1) {
      drupal_set_message(filter_xss(t('Module Active')), 'status');
    }
    else {
      drupal_set_message(filter_xss(t('Module Inactive')), 'status');
    }
  }

  $form['webclinicpro_subscriber_key'] = array(
    '#type' => 'textfield',
    '#title' => t('webClinic Pro Subscriber KEY'),
    '#default_value' => variable_get('webclinicpro_subscriber_key', NULL),
    '#description' => t('The webClinic Pro Subscriber KEY is found in the client administration section of the webClinic Pro website. The KEY is needed to authenticate use of this module.'),
    '#required' => TRUE,
  );

  if (variable_get('webclinicpro_status', 0) == 1) {
    $options = array(
      t('None') => 'None',
      '1' => t('Style #1 - Black Text with Transparent Background'),
      '2' => t('Style #2 - White Text with Transparent Background'),
    );
    $form['webclinicpro_block_seal'] = array(
      '#type' => 'radios',
      '#options' => $options,
      '#title' => t('Enable protection seal?'),
      '#default_value' => variable_get('webclinicpro_block_seal', 0),
      '#description' => t('The webClinic Pro Protection Seal appears at the bottom right on all pages of the website.'),
    );

    $form['webclinicpro_forced_ssl'] = array(
      '#type' => 'checkbox',
      '#title' => t('Force entire website to use SSL?'),
      '#default_value' => variable_get('webclinicpro_forced_ssl', FALSE),
      '#description' => t("This is often referred to as HTTPS Everywhere and is highly encouraged by Google for enhancing website protection.  <em>Don't use Flexible SSL or your website may become inoperable.</em>"),
    );

    $form['webclinicpro_cache'] = array(
      '#type' => 'hidden',
      '#title' => t('Enable website caching?'),
      '#default_value' => variable_get('webclinicpro_cache', FALSE),
      '#description' => t('This will cache html, JavaScript, and Cascading Style Sheets to speed delivery and reduce load on the web server.'),
    );
  }

  $form['cutomtext'] = array(
    '#type' => 'item',
    '#markup' => '<div>Call us for support at 1-800-771-3950 or open a ticket through the customer portal at <a href="https://portal.webclinicpro.com" target="_blank">https://portal.webclinicpro.com</a>.</div>',
    '#weight' => 1,
  );
  // Adjust weight so that you can place it wherever.
  $form['#submit'][] = 'webclinicpro_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Implements hook_admin_settings_submit().
 */
function webclinicpro_admin_settings_submit($form, &$form_state) {
  $data = array(
    "key" => $form['webclinicpro_subscriber_key']['#value'],
    "style" => ((array_key_exists('webclinicpro_block_seal', $form))
      ? $form['webclinicpro_block_seal']['#value']
      : 'Style #1 - Black Text with Transparent Background'),
    "domain" => webclinicpro_get_url(),
  );

  $file = "https://portal.webclinicpro.com/api/validate.php";
  if (function_exists('curl_version')) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $file);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_POST, 2);
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 90);
    curl_setopt($curl, CURLOPT_TIMEOUT, 90);

    $results = curl_exec($curl);
    curl_close($curl);

    $results = json_decode($results);
    if (isset($results->result)) {
      variable_set('webclinicpro_status', 1);
    }
    else {
      variable_set('webclinicpro_status', NULL);
    }
  }
}
